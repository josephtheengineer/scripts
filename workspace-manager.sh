if [ ! -d ~/.cache/workspaces ]; then
	mkdir ~/.cache/workspaces
fi

typeset -i current=1
typeset -i current=$(cat ~/.cache/workspaces/current)
typeset -i mode=0

function switch {
	echo "Switching workspace..."
	typeset -i current=$(cat ~/.cache/workspace/current)
	typeset -i last_mode=$(cat ~/.cache/workspaces/$current)
	echo $1 > ~/.cache/workspaces/current
	typeset -i mode=$(cat ~/.cache/workspaces/$1)
		
	if [ $last_mode != $mode ]; then
		killall conky
		sleep 0.05
		case $mode in
		1)
			# Right panel
			conky -c ~/dotfiles/conky-right.config
		;;
		2)
			# Left panel
			conky -c ~/dotfiles/conky-left.config
		;;
		3)
			# Both panels
			conky -c ~/dotfiles/conky-right.config
			conky -c ~/dotfiles/conky-left.config
		;;
		0)
			echo "0"
		;;
		*)
			echo "error"
		;;
			esac
			sleep 0.05
	fi
	swaymsg workspace $1
}

case $1 in 
	switch)
		switch $2
	;;
	set-mode)
		echo "Seting mode..."
		typeset -i mode=$(cat ~/.cache/workspaces/$current)
		echo "Setting the mode of workspace: "
		echo $current

		if [ ! $mode -eq 0 ]; then
			if [ $mode -eq 1 ] || [ $mode -eq 2 ]; then
				echo 3 > ~/.cache/workspaces/$current
			else
				echo $2 > ~/.cache/workspaces/$current
			fi

		else
			echo $2 > ~/.cache/workspaces/$current
		fi

		sleep 0.05
		switch $current
	;;
	*)
		echo "error: invalid usage"
		echo "workspace-manager switch 1"
		echo "workspace-manager set-mode 0"
		echo "0 == fullscreen"
		echo "1 == right panel shown"
		echo "2 == left panel shown"
		echo "3 == both panels shown"
	;;
esac
