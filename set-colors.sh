source ~/.cache/wal/colors.sh
echo "[colors]
foreground=$foreground
background=$background
color0=$color0
color1=$color1
color2=$color2
color3=$color3
color4=$color4
color5=$color5

[options]
#font=Hack 9
" > ~/.config/termite/config
